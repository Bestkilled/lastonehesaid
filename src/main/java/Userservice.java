


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Userservice {
    private static ArrayList<User> userlist=new ArrayList<>();;
    private static User CurrentUser = null;
    private static User superAdmin = new User("super","super");
    static{
        
    }
    public static boolean addUser(User u){
        userlist.add(u);
        save();
        return true;
    }
    public static boolean delUser(User u){
        userlist.remove(u);
        save();
        return true;
    }
    
    public static boolean delUser(int index){
        userlist.remove(index);
        save();
        return true;
    }
    public static ArrayList<User> getUser(){
        return userlist;
    }
    
    public static User getUser(int index){
        return userlist.get(index);
    }
    public static boolean updateUser(int index, User u){
        userlist.set(index, u);
        save();
        return true;
    }
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos=null;
        try{
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos= new ObjectOutputStream(fos);
            oos.writeObject(userlist);
            oos.close();
           fos.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(Userservice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Userservice.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois=null;
        try{
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois= new ObjectInputStream(fis);
            userlist= (ArrayList<User>) ois.readObject();
            ois.close();
           fis.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(Userservice.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Userservice.class.getName()).log(Level.SEVERE, null, ex);
        }catch (ClassNotFoundException ex) {
            Logger.getLogger(Userservice.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static User authenticate(String userName, String password){
        if(userName.equals("super") && userName.equals("super")){
            return superAdmin;
        }
        for(int i=0; i<userlist.size();i++){
            User user = userlist.get(i);
            if(user.getUsername().equals(userName) &&
                    user.getPassword().equals(password)){
                CurrentUser=user;
                return user;
            }
        }
        return null;
    }
    public static boolean isLogin(){
        return CurrentUser !=null;
    }
    public static User getCurrent(){
        return CurrentUser;
    }
    public static void Logout(){
         CurrentUser=null;
    }
}
